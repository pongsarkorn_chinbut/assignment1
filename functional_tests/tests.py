from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class NewVisitorTest(LiveServerTestCase):  

    def setUp(self):  # เปิดbrowser
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):  #ปิดbrowser
        self.browser.quit()

    def check_for_row_in_name_table(self, row_text):
        # ไว้เรียกใช้เพื่อตรวจสอบแต่ละแถวของตารางชื่อการคำนวน
        table = self.browser.find_element_by_id('calculation_name_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_see_home_page(self):  
        # ทดสอบการเข้ามาใน home page
        # เข้ามาหน้าhome
        self.browser.get(self.live_server_url)

        # เห็นคำว่า'Electricity Charge Calculator'บนส่วน title
        self.assertIn('Electricity Charge Calculator', self.browser.title)

        # เห็นคำว่า'Add or Select Your Electricity Charge Calculation lists'บนส่วน h1
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Add or Select Your Electricity Charge Calculation lists', header_text)

        # ใส่ชื่อการคำนวนใหม่ ในช่องที่เขียนว่า'Enter Your New Calculation Name Here'
        inputbox = self.browser.find_element_by_id('new_calculation_name')
        self.assertEqual(
                inputbox.get_attribute('placeholder'),
                'Enter Your New Calculation Name Here'
        )

        # ผู้ใช้สร้างชื่อการคำนวนใหม่ว่า Thum's home
        inputbox.send_keys('Thum\'s home')

        # ผู้ใช้กด Enter
        inputbox.send_keys(Keys.ENTER)

        # ผู้ใช้จะเห็น 1: Thum\'s home ปรากฏบนตารางรวมชื่อการคำนวน(เป็นลิ้งไปยังหน้าคำนวนในอนาคต)
        self.check_for_row_in_name_table('1: Thum\'s home')
        
        # ผู้ใช้คนที่2ใส่ชื่อการคำนวนใหม่ว่า Chai's home
        inputbox = self.browser.find_element_by_id('new_calculation_name')
        inputbox.send_keys('Chai\'s home')
        inputbox.send_keys(Keys.ENTER)
        # ผู้ใช้คนที่2จะเห็น 1: Thum\'s home และ 2: Chai's home ปรากฏบนตารางรวมชื่อการคำนวน
        self.check_for_row_in_name_table('1: Thum\'s home')        
        self.check_for_row_in_name_table('2: Chai\'s home')
        
        self.fail('Finish the test!')


if __name__ == '__main__': 
    unittest.main(warnings='ignore')
