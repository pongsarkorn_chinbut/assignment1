from django.shortcuts import redirect, render
from electricity_charge.models import Name

def home_page(request):
    if request.method == 'POST':
        # หากมีข้อมูลเข้ามาแบบ POST ให้redirectไป '/' ถ้าไม่ให้อยู่ที่ home.html
        Name.objects.create(text=request.POST['calculation_name'])
        return redirect('/')

    names = Name.objects.all()
    return render(request, 'home.html', {'names': names})
