from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from django.template.loader import render_to_string

from electricity_charge.views import home_page
from electricity_charge.models import Name

class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        # ทดสอบว่า root url เป็นหน้า home page
        found = resolve('/') 
        self.assertEqual(found.func, home_page) 

    def test_home_page_returns_correct_html(self):
        # ทดสอบว่า response ไปให้ browser เป็น html ของ home page 
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.content.decode(), expected_html)
    
    def test_home_page_can_save_a_POST_request(self):
        # ทดสอบรับข้อมูลจาก browser แบบ POST
        request = HttpRequest()
        request.method = 'POST'
        request.POST['calculation_name'] = 'A new calculation name'

        response = home_page(request)

        self.assertEqual(Name.objects.count(), 1)  #ทดสอบดูจำนวนชื่อในโมเดล Name
        new_name = Name.objects.first()  
        self.assertEqual(new_name.text, 'A new calculation name') #ใช่ชื่อนี้ไหม
    
    def test_home_page_redirects_after_POST(self):       
        # ทดสอบการ redirect
        request = HttpRequest()
        request.method = 'POST'
        request.POST['calculation_name'] = 'A new calculation name'

        response = home_page(request)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')


    def test_home_page_only_saves_items_when_necessary(self):
        # ทดสอบว่าจะไม่มีการบันทึกค่าเป็น blank เข้าไปในโมเดล Name(เมื่อไม่มีการ POST)
        request = HttpRequest()
        home_page(request)
        self.assertEqual(Name.objects.count(), 0)

    def test_home_page_displays_all_calculation_name(self):
        Name.objects.create(text='nameAs home')
        Name.objects.create(text='nameBs home')

        request = HttpRequest()
        response = home_page(request)

        self.assertIn('nameAs home', response.content.decode())
        self.assertIn('nameBs home', response.content.decode())

class ItemModelTest(TestCase):

    def test_saving_and_retrieving_name(self): # ทดสอบบันทึกชื่อและแสดงชื่อทั้งหมด
        first_item = Name()
        first_item.text = 'First Name'
        first_item.save()

        second_item = Name()
        second_item.text = 'Second Name'
        second_item.save()

        saved_items = Name.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.text, 'First Name')
        self.assertEqual(second_saved_item.text, 'Second Name')
